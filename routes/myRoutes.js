const express = require("express");
const router = express.Router();
const { getToDoList, createToDoList, updateToDoList, deleteToDoList } = require("../controllers/myController");
const validateToken = require("../middleware/tokenHandler");

router.use(validateToken);
router.route("/").get(getToDoList).post(createToDoList);
router.route("/:id").delete(deleteToDoList).put(updateToDoList);

module.exports = router;

