const express = require("express");
const connectMongoDb = require("./config/connection");
const dotenv = require("dotenv").config();
const errorHandler = require("./middleware/errorHandlers")

connectMongoDb();
const app = express();

const port = 3001 || 3000;

const cors = require('cors');
app.use(cors({
    origin: 'http://localhost:4200'
}));

app.use(express.json());
app.use("/api/test", require("./routes/myRoutes"));
app.use("/api/users", require("./routes/userRoutes"));
app.use(errorHandler);

app.listen(port, () => {
    console.log(`server ${port}`)
}

);