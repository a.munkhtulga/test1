const asyncHandler = require("express-async-handler");
const Test = require("../models/model")

const getToDoList = asyncHandler ( async (req, res) => {
    const toDoList = await Test.find({ user_id: req.user.id });
    res.status(200).json(toDoList);
});

const createToDoList = asyncHandler ( async (req, res) => {
    console.log("sad", req.body)
    const {date, plan} = req.body;
    if(!date || !plan) {
        res.status(400);
        throw new Error("field")
    }
    const toDoList = await Test.create({
        date,
        plan,
        user_id: req.user.id
    });
    res.status(201).json(toDoList);
});

const updateToDoList = asyncHandler ( async (req, res) => {
    const toDoList = await Test.findById(req.params.id);
    if(!toDoList) {
        res.status(404);
        throw new Error("not found")
    }

    const updateToDoList = await Test.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true }
    );

    res.status(200).json(updateToDoList);
});

const deleteToDoList = asyncHandler(async (req, res) => {
    // const deleteToDoList = await Test.findById(req.params.id);
    // console.log(deleteToDoList)
    // console.log(req.params.id)
    // if(!deleteToDoList) {
    //     res.status(404);
    //     throw new Error("not found")
    // }
    await Test.deleteOne({ _id: req.params.id });
    res.status(200).json(deleteToDoList);
});


module.exports = { getToDoList, createToDoList, updateToDoList, deleteToDoList };