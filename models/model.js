const mongoose = require("mongoose");

const schema = mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
    },
    date: {
        type: String,
        required: [true, "add date"],
    },
    plan: {
        type: String,
        required: [true, "add plan"],
    },
}, {
    timestamps: true,
});

module.exports = mongoose.model("ToDoList", schema);